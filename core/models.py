from django.db import models
from stdimage.models import StdImageField

#SIGNALS
from django.db.models import signals
from django.template.defaultfilters import slugify


class Base(models.Model):
    criado = models.DateField(verbose_name='Data de Criação', auto_now_add=True)
    modificado = models.DateField(verbose_name='Data de Atualização', auto_now=True)
    ativo = models.BooleanField(verbose_name='Ativo', default=True)

    class Meta:
        abstract = True


class Produto(Base):
    nome = models.CharField(verbose_name='Nome', max_length=100)
    preco = models.DecimalField(verbose_name='Preço', max_digits=12, decimal_places=2)
    quantidade = models.IntegerField(verbose_name='Quantidade em Estoque')
    imagem = StdImageField(verbose_name='Imagem', upload_to='produtos',\
                           variations={'thumb': (124, 124)})
    slug = models.SlugField(verbose_name='Slug', max_length=100, blank=True,\
                            null=True, editable=False)

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = 'Produtos'


def produto_pre_save(signal, instance, sender, **kwargs):
    instance.slug = slugify(instance.nome)


signals.pre_save.connect(produto_pre_save, sender=Produto)
