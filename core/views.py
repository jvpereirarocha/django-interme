from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import ContatoForm, ProdutoModelForm
from .models import  Produto

def index(request):
    produtos = Produto.objects.all()
    context = {
        'message': 'Produtos',
        'produtos': produtos,
    }
    return render(request, 'index.html', context)


def contato(request):
    form = ContatoForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            nome = form.cleaned_data['nome']
            email = form.cleaned_data['email']
            assunto = form.cleaned_data['assunto']
            mensagem = form.cleaned_data['mensagem']
            form.send_mail()
            messages.success(request, 'E-mail enviado com sucesso!')
            form = ContatoForm()
        else:
            messages.error(request, 'Erro ao enviar e-mail')
            form = ContatoForm()

    context = {
        'message': 'Contato',
        'form': form,
    }
    return render(request, 'contato.html', context)


def produto(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = ProdutoModelForm(request.POST, request.FILES)
            if form.is_valid():
                form.save()
                messages.success(request, 'Produto salvo com sucesso!')
                form = ProdutoModelForm()
            else:
                messages.error(request, 'Não foi possível salvar o produto!')
                form = ProdutoModelForm()
        else:
            form = ProdutoModelForm()

        context = {
            'message': 'Produto',
            'form': form,
        }
        return render(request, 'produto.html', context)
    else:
        return redirect('index')
